﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasled
{
    public class HDD : Storage
    {
        public int Speed { get; set; }
        public int CountOfDirectory { get; set; }
        public int VolumeOfDirectory { get; set; }
    }
}
