﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasled
{
    public class Storage
    {
        Files SFiles;
        Flash SFlash;
        DVD SDvd4;

        private string name;
        private string model;
        private float fullMemory;
        private float comingFiles;
        private float freeMemory;
        //private float memoryOfAllStorage;

        public string Name { set; get; }
        public string Model { set; get; }
        public float FullMemory
        {
            set { fullMemory = value;
                freeMemory = value;
            }
            get { return fullMemory; }
        }
        public float FreeMemory
        {
            get { return freeMemory; }
        }

        public Storage()
        {
            freeMemory = fullMemory;
        }

        public float ComingFiles(float data)
        {
            if (freeMemory >= data)
            {
                comingFiles = data;
                freeMemory -= comingFiles;
                return freeMemory;
            }
            else
            {
                Console.WriteLine("! ! ! Error");
                return 0;
            }
        }

        //public float MemoryOfAllStorage
        //{
        //    set
        //    {
        //        memoryOfAllStorage = SFlash.FullMemory + SDvd4.FullMemory;
        //    }
        //    get
        //    {
        //        return memoryOfAllStorage;
        //    }
        //}

        //float result;
        //int count = 0;
        //int f_c;
        //public float CountOfStorage()
        //{
        //    result = SFiles.AllFiles;
        //    while (SFiles.AllFiles >= 0)
        //    {
        //        while (SFlash.FreeMemory > SFiles.File)
        //        {
        //            SFlash.ComingFiles(SFiles.File);
        //            count++;
        //        }
        //        result = (SFiles.AllFiles - (count * SFiles.File));
        //        f_c = count;
        //        count = 0;
        //        while (SDvd4.FreeMemory > SFiles.File)
        //        {
        //            SDvd4.ComingFiles(SFiles.File);
        //            count++;
        //        }
        //        result = (SFiles.AllFiles - (count * SFiles.File));
        //        f_c += count;
        //        Console.WriteLine("f_c: " + f_c);
        //        return result;
        //    }
        //    Console.WriteLine("result: " + result);
        //    return result;
        //}

    }
}
