﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasled
{
    public class Flash : Storage
    {
        public int Speed { get; set; }
        public int MemoryVolume { get; set; }
    }
}
