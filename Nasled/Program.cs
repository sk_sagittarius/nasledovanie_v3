﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasled
{
    class Program
    {
        static void Main(string[] args)
        {
            Files files = new Files();

            float allFilesProgram;
            int count = 0;
            int countOfFiles;
            float speedOfWriting = 0;
            float speed = 0;

            Flash flash = new Flash
            {
                Name = "New Flash",
                Model = "New flash model",
                FullMemory = 16384,
                Speed = 35,
                
            };

            DVD dvd4 = new DVD
            {
                Name = "New DVD 4,7",
                Model = "New DVD Model",
                //Type = true,
                FullMemory = 4812,
                Speed = 4.7F
            };

            DVD dvd9 = new DVD
            {
                Name = "New DVD 9",
                Model = "New DVD Model",
                //Type = true,
                FullMemory = 9216,
                Speed = 4.7F
            };

            HDD hdd = new HDD
            {
                Name = "New HDD",
                Model = "New HDD Model",
                FullMemory = 1048576, //16tb
                Speed = 110
            };

            // Проверка работы кода на одном классе Flash
            #region Flash test
            //Console.WriteLine("Info:\n" + "Name: " + flash.Name + "\n" + "Model: " + flash.Model + "\n" + "Full memory: " + 
            //    flash.FullMemory + "\n" + "Free memory: " + flash.FreeMemory + "\n\n");

            //flash.ComingFiles(files.File);

            //Console.WriteLine("Info:\n" + "Name: " + flash.Name + "\n" + "Model: " + flash.Model + "\n" + "Full memory: " +
            //    flash.FullMemory + "\n" + "Free memory: " + flash.FreeMemory + "\n\n");
            //int count = 0;
            //while(flash.FreeMemory>files.File)
            //{
            //    flash.ComingFiles(files.File);
            //    count++;
            //}
            //Console.WriteLine("Info:\n" + "Name: " + flash.Name + "\n" + "Model: " + flash.Model + "\n" + "Full memory: " +
            //    flash.FullMemory + "\n" + "Free memory: " + flash.FreeMemory + "\n" + "Count: " + count + "\n\n");

            //float res = count * files.File;
            //Console.WriteLine("Переписано " + count + " файлов, объем свободной памяти: " + flash.FreeMemory + ". Не переписано: " + (files.AllFiles-res) + "\n\n");
            #endregion

            // Проверка работы кода на двух классах
            #region DVD 4 test
            //Console.WriteLine("Info:\n" + "Name: " + dvd4.Name + "\n" + "Model: " + dvd4.Model + "\n" + "Full memory: " +
            //    dvd4.FullMemory + "\n" + "Free memory: " + dvd4.FreeMemory + "\n\n");

            //dvd4.ComingFiles(files.File);

            //Console.WriteLine("Info:\n" + "Name: " + dvd4.Name + "\n" + "Model: " + dvd4.Model + "\n" + "Full memory: " +
            //    dvd4.FullMemory + "\n" + "Free memory: " + dvd4.FreeMemory + "\n\n");
            //count = 0;
            //while (dvd4.FreeMemory > files.File)
            //{
            //    dvd4.ComingFiles(files.File);
            //    count++;
            //}
            //Console.WriteLine("Info:\n" + "Name: " + dvd4.Name + "\n" + "Model: " + dvd4.Model + "\n" + "Full memory: " +
            //    dvd4.FullMemory + "\n" + "Free memory: " + dvd4.FreeMemory + "\n" + "Count: " + count + "\n\n");
            //res = 0;
            //res = count * files.File;
            //Console.WriteLine("Переписано " + count + " файлов, объем свободной памяти: " + dvd4.FreeMemory + ". Не переписано: " + (files.AllFiles - res) + "\n\n");
            #endregion


            float CountOfStorage()
            {
                allFilesProgram = files.AllFiles;
                Console.WriteLine("Объем данных: " + allFilesProgram + "\n\n");
                while (allFilesProgram >= 0)
                {
                    while (flash.FreeMemory > files.File)
                    {
                        flash.ComingFiles(files.File);
                        count++;
                    }
                    allFilesProgram = (allFilesProgram - (count * files.File));
                    speedOfWriting = ((count * files.File) / flash.Speed);
                    Console.WriteLine("Flash\nПереписано " + count + " файлов за " + (speedOfWriting / 60) + " минут. " + "После переписи файлов на флешку остался объем данных: " + allFilesProgram + "\n\n");
                    countOfFiles = count;
                    speed += speedOfWriting;

                    count = 0;
                    while (dvd4.FreeMemory > files.File)
                    {
                        dvd4.ComingFiles(files.File);
                        count++;
                    }
                    allFilesProgram = (allFilesProgram - (count * files.File));
                    speedOfWriting = ((count * files.File) / dvd4.Speed);
                    Console.WriteLine("DVD (4,7)\nПереписано " + count + " файлов за " + (speedOfWriting/60) + " минут. " + "После переписи файлов на диск остался объем данных: " + allFilesProgram + "\n\n");
                    countOfFiles += count;
                    speed += speedOfWriting;

                    count = 0;
                    while (dvd9.FreeMemory > files.File)
                    {
                        dvd9.ComingFiles(files.File);
                        count++;
                    }
                    allFilesProgram = (allFilesProgram - (count * files.File));
                    speedOfWriting = ((count * files.File) / dvd9.Speed);
                    Console.WriteLine("DVD (9)\nПереписано " + count + " файлов за " + (speedOfWriting / 60) + " минут. " + "После переписи файлов на диск остался объем данных: " + allFilesProgram + "\n\n");
                    countOfFiles += count;
                    speed += speedOfWriting;

                    count = 0;
                    while (hdd.FreeMemory > files.File && allFilesProgram > files.File)
                    {
                        hdd.ComingFiles(files.File);
                        allFilesProgram -= files.File;
                        count++;
                    }
                    if (allFilesProgram < files.File) //файлы по 780мб, общий объем 565гб, но ровно не делится, поэтому остаток 580мб
                    {
                        //Console.WriteLine("Все файлы переписаны!\n");
                        //allFilesProgram = 0;
                    }
                    else
                    {
                        allFilesProgram = (allFilesProgram - (count * files.File));
                    }
                    speedOfWriting = ((count * files.File) / hdd.Speed);
                    Console.WriteLine("HDD\nПереписано " + count + " файлов за " + (speedOfWriting / 60) + " минут. " + "После переписи файлов на диск остался объем данных: " + allFilesProgram + "\n\n");
                    countOfFiles += count;
                    speed += speedOfWriting;

                    Console.WriteLine("Количество переписанных файлов: " + countOfFiles + "\n" + "Время, затраченное на запись: " + (speed/60) + " минут\n");
                    return allFilesProgram;
                }
                //Console.WriteLine("result: " + allFilesProgram + "\n");
                return allFilesProgram;
            }

            Console.WriteLine("Общий объем памяти (флешка и диски): " + (flash.FullMemory + dvd4.FullMemory + dvd9.FullMemory + hdd.FullMemory) + "\n\n");

            // Запись на все имеющиеся диски, Flash, DVD (два диска), HDD
            CountOfStorage();

            // запись файлов только на диск HDD
            #region Only HDD
            //count = 0;
            //allFilesProgram = files.AllFiles;
            //while (hdd.FreeMemory > files.File && allFilesProgram > files.File)
            //{
            //    hdd.ComingFiles(files.File);
            //    allFilesProgram -= files.File;
            //    count++;
            //}
            //speedOfWriting = ((count * files.File) / hdd.Speed);
            //Console.WriteLine("Info:\n" + "Name: " + hdd.Name + "\n" + "Model: " + hdd.Model + "\n" + "Full memory: " +
            //    hdd.FullMemory + "\n" + "Free memory: " + hdd.FreeMemory + "\n" + "Count of files: " + count + "\n" + "Time (min): " + (speedOfWriting/60) + "\n");
            #endregion

            Console.ReadLine();
        }
    }
}
